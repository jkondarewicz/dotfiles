#!/bin/bash
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
cp "$(dirname "$0")"/icons/neue_ember/icon_256x256.png /home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=/home/$USER/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
echo 'kitty.desktop' > ~/.config/xdg-terminals.list

mkdir -p ~/.local/share/fonts
cp "$(dirname "$0")"/FiraCode/*.ttf ~/.local/share/fonts/
cp "$(dirname "$0")"/JetBrainsMono/*.ttf ~/.local/share/fonts/
fc-cache -fv

cp "$(dirname "$0")"/kitty.conf ~/.config/kitty/kitty.conf
