local signs = require('utils.signs').diagnostics
local extension = require("utils.ui-utils")

vim.notify = require("notify")

require("ibl").setup()
require('colorizer').setup({})

require('github-theme').setup({
    options = {
        dim_inactive = true,
        styles = {
            functions = "NONE",
            comments = "italic",
            keywords = "bold",
            type = "bold"
        }
    },
})
extension.color("github_dark_high_contrast")

require("lualine").setup({
    extensions = { 'fugitive' },
    options = {
        component_separators = { left = '', right = '' },
        section_separators = { left = '', right = '' },
        globalstatus = true,
    },
    sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'branch', 'diff', 'diagnostics' },
        lualine_c = { {
            'filename',
            path = 0
        } },
        lualine_x = { 'encoding' },
        lualine_y = { extension.lsp_status_bar, 'filetype' },
        lualine_z = { 'tabs', 'location' }
    },
})

require('nvim-tree').setup {
    view = { width = 40 },
    ui = { confirm = { default_yes = true } },
    renderer = {
        group_empty = true
    },
    diagnostics = {
        enable = true,
        show_on_dirs = true,
        icons = {
            hint = signs.HINT,
            info = signs.INFO,
            warning = signs.WARN,
            error = signs.ERROR
        }
    },
}

vim.api.nvim_create_autocmd("LspAttach", {
    callback = function(_)
        require('lualine').refresh()
    end,
})
local nvim = require('nvim-tree.api')

local function focus_on_file(collapse_all)
    local filename = vim.fn.expand('%:p')
    local isFile = vim.fn.filereadable(filename)

    if isFile == 1 then
        local nvim_is_visible = nvim.tree.is_visible()
        if nvim_is_visible then
            if collapse_all then
                nvim.tree.collapse_all()
            end
            nvim.tree.find_file(filename)
        end
    end
end
vim.keymap.set('n', "<leader>wn", function() nvim.tree.toggle({ focus = false }) end,
    { silent = true, noremap = true, desc = "[W]indow [N]avigation toggle" })
vim.keymap.set('n', "<leader>wN", function() nvim.tree.open() end,
    { silent = true, noremap = true, desc = "[W]indow [N]avigation focus" })
local nvim_tree_focus = vim.api.nvim_create_augroup('nvim_tree_focus', { clear = true })
vim.api.nvim_create_autocmd('BufEnter', {
    group = nvim_tree_focus,
    pattern = { '*' },
    desc = 'focus on opened file in nvimtree',
    callback = function(_)
        focus_on_file(false)
    end,
})
