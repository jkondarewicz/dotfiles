local git_utils = require("utils.git-utils")

local function map_git(keymap, func, desc)
    vim.keymap.set({ 'n', 'v' }, keymap, func, { desc = desc })
end

local desc = {
    wg = "[W]indow [G]it open",
    gc = "[G]it [C]ommit",
    gu = "[G]it [U]pdate (fetch && pull)",
    gp = "[G]it [P]ush",
    gl = "[G]it b[L]ame",
    gss = "[G]it [S]tash",
    gsa = "[G]it [S]tash [A]pply",
    gsl = "[G]it [S]tash [L]ist",
    gbn = "[G]it [B]ranch [N]ew",
    gbc = "[G]it [B]ranch [C]hange",
}

map_git("<leader>wg", git_utils.show_git, desc.wg)
map_git("<leader>gc", git_utils.git_commit, desc.gc)
map_git("<leader>gu", git_utils.git_update_and_pull, desc.gu)
map_git("<leader>gp", git_utils.git_push, desc.gp)
map_git("<leader>gl", git_utils.git_blame, desc.gb)
map_git("<leader>gss", git_utils.git_stash, desc.gss)
map_git("<leader>gsa", git_utils.git_stash_apply, desc.gsa)
map_git("<leader>gsl", git_utils.git_stash_list, desc.gsl)
map_git("<leader>gbn", git_utils.git_branch_new, desc.gbn)
map_git("<leader>gbc", git_utils.git_branch_change, desc.gbc)
