local signs = require('utils.signs')
local trouble = require("trouble")
trouble.setup({
    signs = {
        error = signs.diagnostics.ERROR,
        warning = signs.diagnostics.WARN,
        hint = signs.diagnostics.HINT,
        information = signs.diagnostics.INFO,
        other = signs.diagnostics.OTHER,
    },
})

local function map_trouble(keymap, func, desc)
    vim.keymap.set({ "v", "n" }, keymap, func, { desc = desc })
end

local desc = {
    xs = "Toggle symbols for current buffer",
    xd = "Toggle diagnostics for current buffer",
    xD = "Toggle diagnostics for project",
    xl = "Toggle lsp for current buffer",
}

map_trouble("<leader>xs", ":Trouble symbols toggle focus=false win.position=right<cr>", desc.xs)
map_trouble("<leader>xd", ":Trouble diagnostics toggle filter.buf=0 focus=false win.relative=win<cr>", desc.xd)
map_trouble("<leader>xD", ":Trouble diagnostics toggle focus=false win.position=bottom<cr>", desc.xD)
map_trouble("<leader>xl", ":Trouble lsp toggle focus=true win.position=bottom<cr>", desc.xl)
