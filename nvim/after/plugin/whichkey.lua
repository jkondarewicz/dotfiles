require('which-key').register {
    ['<leader>g'] = { name = '[G]it', _ = 'which_key_ignore' },
    ['<leader>c'] = { name = '[C]ode', _ = 'which_key_ignore' },
    ['<leader>t'] = { name = '[T]elescope', _ = 'which_key_ignore' },
    ['<leader>w'] = { name = '[W]indow', _ = 'which_key_ignore' },
    ['<leader>x'] = { name = '[X]trouble', _ = 'which_key_ignore' },
    ['<leader>r'] = { name = '[R]unner', _ = 'which_key_ignore' },
}

