require('todo-comments').setup({
    search = {
        args = {
            "--color=never",
            "--no-heading",
            "--with-filename",
            "--line-number",
            "--column",
            "--ignore-case"
        },
        pattern = [[\b(KEYWORDS):]], -- ripgrep regex
    }
})

vim.keymap.set({ 'v', 'n' }, '<leader>wf', ':TodoTrouble<CR>', { desc = "[W]indow [F]ix/Todo toggle" })
vim.keymap.set({ 'v', 'n' }, '<leader>wF', ':TodoTelescope<CR>', { desc = "[W]indow [F]ix/Todo telescope" })
