local cmp = require('cmp')
local cmp_utils = require('utils.cmp-utils')
local luasnip = require("luasnip")
local types = require("luasnip.util.types")
local luasnip_loaders = require("luasnip.loaders.from_vscode")

luasnip.config.setup({
    ext_opts = {
        [types.choiceNode] = {
            active = {
                virt_text = { { "●" } },
            },
        },
        [types.insertNode] = {
            active = {
                virt_text = { { "●" } },
            },
        },
    },
})

luasnip_loaders.lazy_load()

local function cmp_mappings()
    return {
        ['<C-k>'] = cmp.mapping(function(_) cmp.select_prev_item(cmp_utils.select) end, cmp_utils.all_mappings),
        ['<C-j>'] = cmp.mapping(function(_) cmp.select_next_item(cmp_utils.select) end, cmp_utils.all_mappings),
        ['<C-p>'] = cmp.mapping(function(_) cmp.scroll_docs(-(cmp_utils.scroll_offset)) end, cmp_utils.all_mappings),
        ['<C-n>'] = cmp.mapping(function(_) cmp.scroll_docs(cmp_utils.scroll_offset) end, cmp_utils.all_mappings),
        ['<CR>'] = cmp.mapping(
            function(fallback)
                if (cmp.visible() and cmp.get_selected_entry() ~= nil) then
                    cmp.confirm(cmp_utils.replace)
                else
                    fallback()
                end
            end, cmp_utils.all_mappings),
        ['<C-Space>'] = cmp.mapping(function(_) cmp.complete() end, cmp_utils.all_mappings)
    }
end

cmp.setup({
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    formatting = {
        expandable_indicator = true,
        fields = { "kind", "abbr", "menu" },
        format = require("lspkind").cmp_format({
            mode = "symbol_text",
            maxwidth = 50,
            ellipsis_char = "...",
            show_labelDetails = true,
            before = function(entry, vim_item)
                local source = (entry.source.name)
                vim_item.menu = '[' .. source .. '] ' .. (vim_item.menu or '')
                return vim_item
            end
        })
    },
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'nvim_lua' },
        { name = 'luasnip', keyword_length = 2 },
        { name = 'buffer', keyword_length = 3 },
    },
    mapping = cmp_mappings(),
})

cmp.setup.cmdline({ '/', '?' }, {
    sources = {
        { name = 'buffer' }
    }
})

cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})

local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on(
    'confirm_done',
    function()
        cmp_autopairs.on_confirm_done()
    end
)

local luasnip_modes = { 'i', 's', 'n', 'v' }
vim.keymap.set(luasnip_modes, '<C-x>', cmp_utils.luasnip_next)
vim.keymap.set(luasnip_modes, '<C-z>', cmp_utils.luasnip_prev)
vim.keymap.set(luasnip_modes, '<C-e>', cmp_utils.luasnip_change_choice)
