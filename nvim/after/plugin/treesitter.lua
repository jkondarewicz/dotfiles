require 'nvim-treesitter.configs'.setup({
    modules = {},
    ignore_install = {},
    ensure_installed = "all",
    sync_install = false,
    auto_install = true,
    highlight = {
        enable = true,

        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
    textobjects = {
        select = {
            enable = true,
            lookahead = true,
            keymaps = {
                ["ip"] = "@parameter.inner",
                ["op"] = "@parameter.outer",
                ["ia"] = "@assingment.inner",
                ["oa"] = "@assingment.outer",
                ["ib"] = "@block.inner",
                ["ob"] = "@block.outer",
                ["if"] = "@function.inner",
                ["of"] = "@function.outer",
                ["ic"] = "@conditional.inner",
                ["oc"] = "@conditional.outer",
            }
        },
        swap = {
            enable = true,
            swap_next = {
                ["<leader>p"] = "@parameter.inner",
            },
            swap_previous = {
                ["<leader>P"] = "@parameter.inner"
            }
        },
        move = {
            enable = true,
            set_jumps = true,
            goto_previous_start = {
                ["[p"] = "@parameter.inner",
                ["[f"] = "@function.inner",
            },
            goto_next_start = {
                ["]p"] = "@parameter.inner",
                ["]f"] = "@function.inner",
            }
        },
    }
})

local ts_repeat_move = require "nvim-treesitter.textobjects.repeatable_move"

vim.keymap.set({ "n", "x", "o" }, ";", ts_repeat_move.repeat_last_move_next)
vim.keymap.set({ "n", "x", "o" }, ",", ts_repeat_move.repeat_last_move_previous)

vim.keymap.set({ "n", "x", "o" }, "f", ts_repeat_move.builtin_f_expr, { expr = true })
vim.keymap.set({ "n", "x", "o" }, "F", ts_repeat_move.builtin_F_expr, { expr = true })
vim.keymap.set({ "n", "x", "o" }, "t", ts_repeat_move.builtin_t_expr, { expr = true })
vim.keymap.set({ "n", "x", "o" }, "T", ts_repeat_move.builtin_T_expr, { expr = true })
