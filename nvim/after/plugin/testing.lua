vim.g["test#neovim#start_normal"] = 1
vim.g["test#strategy"] = "neovim"

local neotest = require("neotest")

local function map(key, func, desc)
    vim.keymap.set({ "n", "v" }, key, func, { desc = desc })
end

local function open_test_summary()
    neotest.summary.open()
end
neotest.setup({
    adapters = {
        require("neotest-java")({ ignore_wrapper = false}),
        require("neotest-vim-test")
    }
})

map("<leader>wt", function()
    neotest.summary.toggle()
end, "[W]indow [T]est toggle")
