require("neodev").setup({
	override = function(root_dir, library)
		if root_dir:find("/var/workspace/dotfiles/nvim", 1, true) == 1 then
			library.enabled = true
			library.plugins = true
		end
	end,
})
local lsp_zero = require("lsp-zero")
local autopairs = require("nvim-autopairs")
local mason = require("mason")
local mason_lspconfig = require("mason-lspconfig")
local lsp_utils = require("utils.lsp-utils")

local default_on_attach = function(_, bufnr)
    local function lsp_map(mode, key, func, desc)
        vim.keymap.set(mode, key, func, { buffer = bufnr, desc = desc, silent = true })
    end

    lsp_map('n', "<leader>cr", lsp_utils.rename, '[C]ode [R]ename')
    lsp_map('n', "<leader>ca", lsp_utils.code_actions, '[C]ode [A]ction')
    lsp_map("n", "gd", lsp_utils.go_to_definition, "[G]o to [D]efinition")
    lsp_map("n", "gr", lsp_utils.go_to_references, "[G]o to [R]eferences")
    lsp_map("n", "gI", lsp_utils.go_to_implementations, "[G]o to [I]mplementations")
    lsp_map({ "n", "v" }, "<C-A-r>", lsp_utils.rename, "[R]e[N]ame")
    lsp_map({ "n", "v" }, "<C-A-i>", lsp_utils.code_actions, "[C]ode [A]ction")
end

autopairs.setup()
lsp_utils.setup_neodev()
lsp_zero.on_attach(default_on_attach)
mason.setup()
mason_lspconfig.setup({
    ensure_installed = lsp_utils.lsp_servers,
    handlers = {
        lsp_zero.default_setup,
        lua_ls = function()
            local lua_opts = lsp_zero.nvim_lua_ls()
            lua_opts.settings = {
                Lua = {
                    completion = {
                        callSnippet = "Replace",
                    },
                },
            }
            require("lspconfig").lua_ls.setup(lua_opts)
        end,
        volar = function()
            require 'lspconfig'.volar.setup {
                on_attach = function(client, _)
                    vim.keymap.set('n', 'gk', function()
                        client.request("volar/client/findFileReference",
                            {
                                textDocument = {
                                    uri = vim.fn.expand('%:p')
                                }
                            },
                            function(_, callback)
                                if callback ~= nil then
                                    local references = {}
                                    local len = 0
                                    for _, value in pairs(callback) do
                                        local reference = {
                                            file = string.gsub(value.uri,
                                                'file://', ''),
                                            range = {
                                                row = value.range.start
                                                    .line,
                                                col = value.range.start
                                                    .character
                                            }
                                        }
                                        table.insert(references, reference)
                                        len = len + 1
                                    end
                                    if len == 1 then
                                        local selected = references[1]
                                        vim.cmd('e ' ..
                                            selected.file ..
                                            '|call cursor(' ..
                                            selected.range.row ..
                                            ',' .. selected.range.col .. ')')
                                    else
                                        vim.ui.select(
                                            references,
                                            {
                                                prompt = "SFC references",
                                                format_item = function(
                                                    item)
                                                    return item.file
                                                end
                                            },
                                            function(selected)
                                                if selected ~= nil then
                                                    vim.cmd('e ' ..
                                                        selected.file ..
                                                        '|call cursor(' ..
                                                        selected.range
                                                        .row ..
                                                        ',' ..
                                                        selected.range
                                                        .col ..
                                                        ')')
                                                end
                                            end
                                        )
                                    end
                                end
                            end,
                            0
                        )
                    end)
                end,
                filetypes = { 'typescript', 'javascript', 'javascriptreact', 'typescriptreact', 'vue', 'json' }
            }
        end,
        jdtls = lsp_zero.noop,
    },
})

require("conform").setup({
    formatters_by_ft = {
        yaml = { 'yamlfmt' }
    },
})
local tsserver = nil
local volar = nil
vim.api.nvim_create_autocmd("LspAttach", {
    callback = function(args)
        local client = vim.lsp.get_client_by_id(args.data.client_id)
        if client ~= nil and client.name == 'volar' then
            volar = client
            if tsserver ~= nil then
                tsserver.stop()
            end
        elseif client ~= nil and client.name == 'tsserver' then
            tsserver = client
            if volar ~= nil then
                tsserver.stop()
            end
        end
    end,
})

-- local tsserver = nil
-- local volar = nil
vim.api.nvim_create_autocmd("LspAttach", {
	callback = function(args)
        local bufnr = args.buf
        require("utils.cmp-utils").attach_lsp_signature(bufnr)
		local client = vim.lsp.get_client_by_id(args.data.client_id)
		if client.name == 'volar' then
			volar = client
			if tsserver ~= nil then
				tsserver.stop()
			end
		elseif client.name == 'tsserver' then
			tsserver = client
			if volar ~= nil then
				tsserver.stop()
			end
		end
	end,
})

vim.keymap.set('n', '<C-A-f>', function()
    require('conform').format({ bufnr = 0, lsp_fallback = true }, function(err, edited)
        if (err) then
            vim.notify('formatting error' .. err)
        end
        if (edited) then
            vim.notify("formatted file: " .. vim.fn.expand('%'))
        end
    end)
end)
