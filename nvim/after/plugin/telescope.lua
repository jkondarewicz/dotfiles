local builtin = require('telescope.builtin')
local actions = require('telescope.actions')
local utils = require("telescope.utils")
local telescope_extension = require("utils.telescope-utils")

require("telescope").setup({
    defaults = {
        dynamic_preview_title = true,
        path_display = function(_, path)
            local tail = utils.path_tail(path)
            return string.format("%s (%s)", tail, path)
        end,
        mappings = {
            i = {
                ["<C-k>"] = actions.move_selection_previous,
                ["<C-j>"] = actions.move_selection_next
            }
        },
    },
})

local function telescope_builtin_map(keymap, func, desc)
    vim.keymap.set({ 'n', 'v' }, keymap, function() func() end, { desc = desc })
end

local desc = {
    ff = "Find files",
    lg = "Live grep",
    to = "[T]elescope [O]verview",
    tb = "[T]elescope [B]uffers",
    tp = "[T]elescope [P]roject files within current buf dir",
    tf = "[T]elescope [F]ind in files within current buf dir"
}

telescope_builtin_map("<C-p>", builtin.find_files, desc.ff)
telescope_builtin_map("<C-f>", builtin.live_grep, desc.lg)
telescope_builtin_map("<C-b>", builtin.buffers, desc.tb)
telescope_builtin_map("<leader>to", builtin.builtin, desc.to)
telescope_builtin_map("<leader>tb", builtin.buffers, desc.tb)
telescope_builtin_map("<leader>tp", telescope_extension.find_files_from_current_dir, desc.tp)
telescope_builtin_map("<leader>tf", telescope_extension.live_grep_from_current_dir, desc.tf)
vim.keymap.set('v', '<C-f>', telescope_extension.find_based_on_current_selection)
