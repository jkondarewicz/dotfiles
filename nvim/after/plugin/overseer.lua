local os = require("overseer")
local extension = require("utils.overseer-utils")

local function map_overseer(keymap, func, desc)
    vim.keymap.set({"v", "n"}, keymap, func, { desc = desc })
end

os.setup({
    templates = { "make", "java.maven_run_task" },
    default_template_prompt = 'always',
    task_list = {
        bindings = {
            ['<C-b>'] = ":OverseerBuild<CR>",
            ['<C-d>'] = ":OverseerRun<CR>",
            ['<C-q>'] = ":OverseerClose<CR>"
        }
    },
    log = {
        {
            type = "file",
            filename = "overseer.log",
            level = vim.log.levels.DEBUG, -- or TRACE for max verbosity
        },
    },
})

local desc = {
    wr = "[W]indow [R]unner toggle",
    rb = "[R]unner [B]uild",
    rr = "[R]unner [B]uild",
}

map_overseer("<leader>wr", extension.toggle, desc.wr)
map_overseer("<leader>rb", extension.build, desc.rb)
map_overseer("<leader>rr", extension.run, desc.rr)
