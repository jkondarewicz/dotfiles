local signs = require('utils.signs').diagnostics
vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 0
vim.opt.autoindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv('HOME') .. '/.vim/undodir'

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "120"

vim.g.mapleader = " "

vim.o.laststatus = 3

vim.o.guifont = "FiraCode Mono"
vim.o.completeopt = 'menuone,noselect'
local bufleave = vim.api.nvim_create_augroup('save_on_buf_leave', { clear = true })
vim.api.nvim_create_autocmd('BufLeave', {
    group = bufleave,
    pattern = { '*' },
    desc = 'save file on buf leave',
    callback = function(event)
        local buftype = vim.api.nvim_buf_get_option(event.buf, 'buftype')
        local isBuffNotWritable = buftype == 'nowrite' or buftype == 'nofile'
        local filename = vim.fn.expand("%:p")
        local isWriteable = vim.fn.filewritable(filename)
        if isWriteable == 1 and not isBuffNotWritable then
            vim.cmd(":w!")
        end
    end,
})
vim.diagnostic.config({
    virtual_text = {
        prefix = function(e)
            local t = e.severity
            if t == vim.diagnostic.severity.ERROR then
                return signs.ERROR
            elseif t == vim.diagnostic.severity.HINT then
                return signs.HINT
            elseif t == vim.diagnostic.severity.INFO then
                return signs.INFO
            elseif t == vim.diagnostic.severity.WARN then
                return signs.WARN
            end
            return signs.OTHER
        end
    },
    -- signs = false
    signs = {
        text = {
            [vim.diagnostic.severity.ERROR] = signs.ERROR,
            [vim.diagnostic.severity.HINT] = signs.HINT,
            [vim.diagnostic.severity.INFO] = signs.INFO,
            [vim.diagnostic.severity.WARN] = signs.WARN
        }
    },
})
vim.keymap.set('v', '<tab>', 'maomeo>`eV`a')
vim.keymap.set('v', '<S-tab>', 'maomeo<`eV`a')
vim.keymap.set('i', '<S-tab>', '<BS>');
