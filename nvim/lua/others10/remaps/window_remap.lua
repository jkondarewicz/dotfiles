local silent = { silent = true }
vim.keymap.set('n', '<A-h>', ':TmuxNavigateLeft<CR>', silent)
vim.keymap.set('n', '<A-j>', ':TmuxNavigateDown<CR>', silent)
vim.keymap.set('n', '<A-k>', ':TmuxNavigateUp<CR>', silent)
vim.keymap.set('n', '<A-l>', ':TmuxNavigateRight<CR>', silent)

vim.keymap.set('n', '+', '<C-w>+')
vim.keymap.set('n', '_', '<C-w>-')
vim.keymap.set('n', '<', '<C-w><')
vim.keymap.set('n', '>', '<C-w>>')

--tabs
local desc = function(desc)
    local M = {}
    M.desc = desc
    return M
end

vim.keymap.set({ 'v', 'n' }, '<C-t>c', ':tabnew %<cr>', desc('[T]ab [C]reate new'))
vim.keymap.set({ 'v', 'n' }, '<C-t>n', ':tabnext<cr>', desc('[T]ab [N]ext'))
vim.keymap.set({ 'v', 'n' }, '<C-t>p', ':tabprevious<cr>', desc('[T]ab [P]revious'))
vim.keymap.set({ 'v', 'n' }, '<C-t>s', ':sp<CR>', desc('[T]ab [S]plit'))
vim.keymap.set({ 'v', 'n' }, '<C-t>v', ':vsp<CR>', desc('[T]ab [V]ertical split'))
vim.keymap.set({ 'v', 'n' }, '<C-t>q', ':tabclose<cr>', desc('[T]ab [C]lose'))

for i = 1, 9 do
    vim.keymap.set({ 'v', 'n' }, '<C-t>' .. i, ':tabn ' .. i .. '<cr>', desc('[T]ab [' .. i .. '] page'))
end
