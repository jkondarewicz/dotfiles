vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-j>", "<C-d>zz")
vim.keymap.set("n", "<C-k>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

--paste without changing clipboard context
vim.keymap.set({ "x", "v" }, "P", "\"_dP")
vim.keymap.set({ "n", "v" }, "Y", "\"+y")
vim.keymap.set("n", "Y", "\"+Y")
vim.keymap.set({ "n", "v" }, "D", "\"_d")

vim.keymap.set("i", "<C-c>", "<Esc>")
vim.keymap.set({ "v", 'n' }, "<C-q>", "<C-w>q")

--saving using shortcut
vim.keymap.set({ "n", "v" }, "<C-s>", ":w!<CR>")
vim.keymap.set("i", "<C-s>", '<ESC>' .. ":w!<CR>" .. 'a')

----walking in insert mode!!!!
vim.keymap.set('i', '<A-j>', '<Down>')
vim.keymap.set('i', '<A-k>', '<Up>')
vim.keymap.set('i', '<A-h>', '<Left>')
vim.keymap.set('i', '<A-l>', '<Right>')

--make commentary awesome
local comment_cmd = "msome:Commentary<CR><ESC>`eV`s"
vim.keymap.set("n", "<C-_>", ':Commentary<CR>', { noremap = true, silent = true, desc = "comment line" })
vim.keymap.set("v", "<C-_>", comment_cmd, { noremap = true, silent = true, desc = "comment lines" })

vim.keymap.set({ "n", "v" }, "<C-n>", "]mzz")
vim.keymap.set({ "n", "v" }, "<C-m>", "[mzz")

vim.keymap.set('i', '<C-j>', '<ESC>o', {})
vim.keymap.set('i', '<C-k>', '<ESC>O', {})

vim.keymap.set('n', '<C-o>', '<C-o>zz')
vim.keymap.set('n', '<C-i>', '<C-i>zz')
