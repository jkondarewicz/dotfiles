vim.cmd([[packadd packer.nvim]])

return require("packer").startup(function(use)
    use("wbthomason/packer.nvim")
    use 'nvim-lua/plenary.nvim'
    use({
        "nvim-telescope/telescope.nvim",
        tag = "0.1.7"
    })
    use({
        "folke/which-key.nvim",
        config = function()
            require("which-key").setup()
        end,
    })

    --lsp
    use("nvim-treesitter/nvim-treesitter", { run = ":TSUpdate" })
    use({
        "nvim-treesitter/nvim-treesitter-textobjects",
        after = "nvim-treesitter",
        requires = "nvim-treesitter/nvim-treesitter",
    })
    use({
        "VonHeikemen/lsp-zero.nvim",
        branch = "v3.x",
        requires = {
            --- Uncomment these if you want to manage the language servers from neovim
            { "williamboman/mason.nvim" },
            { "williamboman/mason-lspconfig.nvim" },

            -- LSP Support
            { "neovim/nvim-lspconfig" },
            { "mfussenegger/nvim-jdtls" },

            -- Autocompletion
            { "L3MON4D3/LuaSnip", tag = "v2.*", build = "make install_jsregexp" },
            { "hrsh7th/nvim-cmp" },
            { "hrsh7th/cmp-nvim-lsp" },
            { "hrsh7th/cmp-cmdline" },
            { "hrsh7th/cmp-buffer" },
            { "hrsh7th/cmp-path" },
            { "hrsh7th/cmp-nvim-lua" },
            { "ray-x/lsp_signature.nvim" },
            { "saadparwaiz1/cmp_luasnip" },
            { "rafamadriz/friendly-snippets" },
            { "onsails/lspkind-nvim" },
            { "folke/neodev.nvim" },
            { "windwp/nvim-autopairs" },

            -- Formatter
            { "stevearc/conform.nvim" }
        },
    })

    use {
        "nvim-neotest/neotest",
        requires = {
            "nvim-lua/plenary.nvim",
            "antoinemadec/FixCursorHold.nvim",
            "nvim-treesitter/nvim-treesitter",
            "vim-test/vim-test",
            "nvim-neotest/neotest-vim-test",
            "nvim-neotest/nvim-nio",
            "rcasia/neotest-java",
        }
    }

    --utilities
    use("folke/trouble.nvim")
    use("folke/todo-comments.nvim")
    use("tpope/vim-commentary")
    use("tpope/vim-fugitive")
    use("christoomey/vim-tmux-navigator")
    use("stevearc/overseer.nvim")

    --ui
    use("nvim-tree/nvim-tree.lua")
    use('projekt0n/github-nvim-theme')
    use("rcarriga/nvim-notify")
    use("stevearc/dressing.nvim")
    use('NvChad/nvim-colorizer.lua')
    use("nvim-tree/nvim-web-devicons")
    use("nvim-lualine/lualine.nvim")
    use("lukas-reineke/indent-blankline.nvim")
end)
