local telescope = require("telescope.builtin")

local M = {}

M.lsp_servers = {
	"docker_compose_language_service",
	"tsserver",
	"volar",
	"jdtls",
	"bashls",
	"html",
	"lua_ls",
	"cssls",
	"dockerls",
	"yamlls",
	"ansiblels"
}

function M.attached_lsp_as_string()
    local active_lsp_clients = vim.lsp.get_clients({ bufnr = 0 })
    local clients = ""
    for _, v in pairs(active_lsp_clients) do
        clients = clients .. v.name .. ", "
    end
    if clients ~= "" then
        return clients:sub(1, -3)
    else
        return nil
    end
end

function M.setup_neodev()
    require("neodev").setup({
        override = function(root_dir, library)
            if root_dir:find("/var/workspace/dotfiles/nvim", 1, true) == 1 then
                library.enabled = true
                library.plugins = true
            end
        end
    })
end

function M.go_to_definition()
    telescope.lsp_definitions()
end

function M.go_to_references()
    telescope.lsp_references()
end

function M.go_to_implementations()
    telescope.lsp_implementations()
end

function M.rename()
    vim.lsp.buf.rename()
end

function M.code_actions()
    vim.lsp.buf.code_action()
end


return M
