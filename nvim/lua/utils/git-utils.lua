local M = {}

function M.get_current_branch()
    return vim.fn.system "git branch --show-current | tr -d '\n'"
end

function M.show_git()
    vim.cmd("Git")
end

function M.git_commit()
    vim.cmd("Git commit")
end

function M.git_blame()
    vim.cmd("Git blame")
end

function M.git_stash()
    vim.cmd("Git stash")
end

function M.git_stash_apply()
    vim.cmd("Git stash apply")
end

function M.git_stash_list()
    vim.cmd("Git stash list")
end

function M.git_update()
    vim.cmd("Git fetch --prune")
end

function M.git_update_and_pull()
    M.git_update()
    vim.cmd("Git pull")
end

function M.git_push()
    local branch = M.get_current_branch()
    if branch ~= nil then
        vim.cmd("Git push --set-upstream origin " .. branch)
    end
end

function M.git_branch_new()
    vim.ui.input({ prompt = "Name of the new branch" }, function(branch_name)
        if branch_name ~= nil and branch_name ~= "" then
            vim.cmd("Git checkout -B " .. branch_name)
        end
    end)
end

function M.git_branch_change()
    M.git_update()
    local current_branch = M.get_current_branch()
    local branches_str = vim.fn.system "git branch -a | cut -c 3-"
    local branches = {}
    for line in branches_str:gmatch("([^\n]*)\n?") do
        local local_branch = string.gsub(line, "(remotes/.*/)", "")
        if line and line ~= "" and local_branch ~= current_branch then
            if line:find("^remotes/") and branches[local_branch] == nil then
                branches[local_branch] = { branch = local_branch, is_remote = true }
            else
                branches[local_branch] = { branch = local_branch, is_remote = false }
            end
        end
    end
    local b = {}
    for _,v in pairs(branches) do
        table.insert(b, v)
    end
    vim.ui.select(b, {
        prompt = "Choose branch (current: " .. current_branch .. ")",
        format_item = function(item) return "(" .. (item.is_remote and "R" or "L") .. ") -> " .. item.branch end
    }, function(selected_branch)
        if selected_branch ~= nil then
            if selected_branch.is_remote then
                vim.cmd("Git switch " .. selected_branch.branch)
            else
                vim.cmd("Git checkout " .. selected_branch.branch)
            end
        end
    end)
end

return M
