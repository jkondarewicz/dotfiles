local M = {}

local builtin = require("telescope.builtin")
local utils = require("telescope.utils")
local editor = require("utils.editor")

function M.find_files_from_current_dir()
    builtin.find_files({ cwd = utils.buffer_dir() })
end

function M.live_grep_from_current_dir()
    builtin.live_grep({ cwd = utils.buffer_dir() })
end

function M.find_based_on_current_selection()
    local search_keyword = editor.get_selection()
    if search_keyword and search_keyword ~= "" then
        builtin.grep_string({ search = search_keyword })
    end
end

return M
