local signs = {}
signs.diagnostics = {
    INFO = '',
    HINT = '',
    WARN = '',
    ERROR = '',
    OTHER = ''
}
return signs
