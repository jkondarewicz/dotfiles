local cmp = require('cmp')
local luasnip = require("luasnip")

local M = {}

M.scroll_offset = 4
M.all_mappings = { 'i', 'c', 'x', 's' }
M.select = { behavior = cmp.SelectBehavior.Select }
M.replace = { behavior = cmp.ConfirmBehavior.Replace, select = false }

function M.attach_lsp_signature(bufnr)
    local settings = {
        -- floating_window = false,
        hint_prefix = "current argument = "
    }
    require("lsp_signature").on_attach(settings, bufnr)
end

local function luasnip_jump(offset)
    if luasnip.jumpable(offset) then
        luasnip.jump(offset)
    end
end

function M.luasnip_next()
    luasnip_jump(1)
end

function M.luasnip_prev()
    luasnip_jump(-1)
end

function M.luasnip_change_choice()
    if (luasnip.choice_active()) then
        luasnip.changeChoice(1)
    end
end

return M
