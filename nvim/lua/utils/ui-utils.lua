local lsp_extension = require("utils.lsp-utils")

local M = {}

---@param colorscheme string: colorscheme name
function M.color(colorscheme)
    vim.cmd('colorscheme ' .. colorscheme)
end

function M.lsp_status_bar()
    local lsp_attached = lsp_extension.attached_lsp_as_string()
    if lsp_attached then
        return "LSP: " .. lsp_attached
    end
    return ""
end

return M
