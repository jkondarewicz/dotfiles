local M = {}

function M.toggle()
    vim.cmd("OverseerToggle right")
end

function M.open()
    vim.cmd("OverseerOpen right")
end

function M.build()
    M.open()
    vim.cmd("OverseerBuild")
end

function M.run()
    M.open()
    vim.cmd("OverseerRun")
end

return M
