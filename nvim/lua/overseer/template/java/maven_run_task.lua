return {
    name = "Run maven application",
    builder = function(params)
        local profiles = params.profile
        local args = ''
        if profiles then
            args = '-Dspring-boot.run.profiles=' .. profiles
        end
        return {
            cmd = { 'mvn' },
            cwd = vim.fn.getcwd(),
            args = { 'spring-boot:run', args }
        }
    end,

    desc = "Run maven application by command mvn",
    params = {
        profile = {
            type = "string",
            name = "Run application with defined profiles",
            order = 1,
            validate = function()
                return true
            end,
            optional = true
        }
    },
    priority = 50,
    condition = {
        callback = function(search)
            local root_dir = vim.fn.getcwd()
            local mvn_file = root_dir .. '/pom.xml'
            local is_file = vim.fn.filereadable(mvn_file)
            if is_file == 1 then
                return true
            end
            return false
        end
    }
}
