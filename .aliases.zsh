local workspace_dir=/var/workspace
alias ll='ls -l'
alias vim='nvim'
alias workspace="cd $workspace_dir"
alias tmux="tmux -2"
alias tn="tmux new"
alias ta="tmux attach"

nvim-apartment() {
    cd "$workspace_dir/apartments/$1"
    nvim .
}
_nvim-apartment() {
    full_path_projects=($workspace_dir/apartments/*)
    projects=()
    for project in $full_path_projects
    do
        projects+=(`basename $project`)
    done
    compadd $projects
}

compdef _nvim-apartment nvim-apartment

nvim-project() {
    cd "$workspace_dir/$1"
    nvim .
}
_nvim-project() {
    full_path_projects=($workspace_dir/*)
    projects=()
    for project in $full_path_projects
    do
        projects+=(`basename $project`)
    done
    compadd $projects
}

compdef _nvim-project nvim-project
