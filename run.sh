#!/bin/bash
cdir=`pwd`

# cleaning
rm -rf "$HOME/.local/share/nvim"
rm -rf "$HOME/.config/nvim"
rm -rf "$HOME/.tmux.conf"

# install
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
curl -s "https://get.sdkman.io" | bash
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
sudo apt -y install tmux

# create symlinks
ln -sf "$cdir/nvim" "$HOME/.config/nvim"
ln -sf "$cdir/.tmux.conf" "$HOME/.tmux.conf"
ln -sf "$cdir/.zshrc" "$HOME/.zshrc"
ln -sf "$cdir/.aliases.zsh" "$HOME/.aliases.zsh"

#install terminal
./terminal/install_terminal.sh
