export ZSH="$HOME/.oh-my-zsh"
export PATH="$HOME/.local/bin:$HOME/neovim/bin:$PATH"
ZSH_THEME="agnoster"

plugins=(
    git
    zsh-autosuggestions
    zsh-syntax-highlighting
    npm
)
source $ZSH/oh-my-zsh.sh
source ~/.aliases.zsh

export LANG=en_US.UTF-8
export EDITOR='nvim'

stty -ixon
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
